# Oliver  von Zweydorff
# Version 0.2
# Dezember 2022

import os
import shutil
import re

# contentpath = __file__.split('.')[0]
contentpath = os.path.realpath(os.path.dirname(__file__))
parentpath = os.path.abspath(os.path.join(contentpath, os.pardir))
assetspath = os.path.abspath(os.path.join(parentpath, "assets"))

img_pre_strt_str = "!["
img_pre_end_str = "("
img_end_str = ")"

print("-----------------------------------------------------------------------")
print("THIS TOOL CREATES A ASSET FOLDER FOR YOUR MARKDOWN POST.")
print("IN THE MARKDOWN FILE IT CHANGES ALL IMAGE PATHS ACCORDINGLY")
print("-----------------------------------------------------------------------")

# VARIABLES
# MARKDOWN FILE
img_idx_lst = []
img_idx_pre_lst = []
img_idx_end_lst = []

old_img_ref_lst = []
new_img_ref_lst = []

md_string = ""

new_md_name = ''
new_file_path = ''
new_file_path_md = ''

# IMAGES
ext_img_str = ""
img_pre_str = ""
old_img_path = ""
new_img_path = ""
new_img_path_md = ""
new_img_path_md_img = ""

old_img_ref = ""
new_img_ref = ""

do_load_images = False
image_path = ''
image_folder_path = ''
referenced_img_names = []
copied_img_names = []
new_img_dir = ""

# MATERIALS
ext_mat_str = ""
mat_pre_str = ""
old_mat_path = ""
new_mat_path = ""
new_mat_path_md = ""
new_mat_path_md_img = ""

old_mat_ref = ""
new_mat_ref = ""

do_load_materials = False
mat_path = ''
mat_folder_path = ''
copied_mat_names = []


# FUNCTIONS

#..... get md file
# Get the Markdown file
md_path = input(r"//  Drag your markdown file here: ")
while not os.path.isfile(md_path):
    md_path = input(r"//  Drag your markdown file here: ") 

md_name = os.path.basename(md_path).split('/')[-1]

print(">>>>  Your markdown file is: " + md_name)
print("-----------------------------------------------------------------------")

#..... rename md file and add subdir
# ---------------------------
# Set the new name and path

rename_md = input("//  Do you want to rename the file? All subfolders will get that new name. Else they get the current name (Y/N): ")
if rename_md == "N" or rename_md == "n":
    print('>>>>  Your new markdown files name will be: ' + md_name + '')
    print("-----------------------------------------------------------------------")
elif rename_md == "Y" or rename_md == "y":
    new_md_name = input("//  Enter your new name. If you want to create a subfolders type something like `subfolder/filename.md`. Else just `filename.md`: ")
    if not new_md_name.lower().endswith('.md'):
        new_md_name = new_md_name + '.md'
    print('>>>>  Your new markdown files name will be: ' + new_md_name + '')
    print("-----------------------------------------------------------------------")
    md_name = new_md_name.split('/')[-1]
    if os.path.dirname(new_md_name): new_file_path = "/"+ os.path.dirname(new_md_name)
    else: new_file_path = ''
    new_file_path_md = new_file_path.replace('\'', '/')
    np = new_file_path.replace('\'', '/').split('/')
    new_file_path = os.path.join(*np)
    
    # print("New file path: " + new_file_path )
else:
    # Like don't rename N 
    print('>>>>  Your new markdown files name will be: ' + md_name + '')
    print("-----------------------------------------------------------------------")


#..... get img folder
# ---------------------------
# Load the images
has_images = input(r"//  Are there images to copy? (Y/N): ")
if has_images == "N" or has_images == "n":
    do_load_images = False
elif has_images == "Y" or has_images == "y":
    do_load_images = True
else: do_load_images = False
print("-----------------------------------------------------------------------")

if do_load_images:
    image_path = input(r"//  Drag your folder or the first image here to get its path: ")
    ## Show the old folder path
    if(os.path.isfile(image_path)): 
        image_folder_path = os.path.abspath(os.path.join(image_path, os.pardir))
    elif(os.path.isdir(image_path)):
        image_folder_path = image_path
    print(">>>>  Your image folder is: " + image_folder_path)
    print("-----------------------------------------------------------------------")

#..... get mat folder
# Load the material
has_materials = input(r"//  Are there materials (files) to copy? (Y/N): ")
if has_materials == "N" or has_materials == "n":
    do_load_materials = False
elif has_materials == "Y" or has_materials == "y":
    do_load_materials = True
else: do_load_materials = False
print("-----------------------------------------------------------------------")


if do_load_materials:
    mat_path = input(r"//  Drag your folder or the first material here to get its path: ")
    ## Show the old folder path
    if(os.path.isfile(mat_path)): 
        mat_folder_path = os.path.abspath(os.path.join(mat_path, os.pardir))
    elif(os.path.isdir(mat_path)):
        mat_folder_path = mat_path
    print(">>>>  Your material folder is: " + mat_folder_path)
    print("-----------------------------------------------------------------------")
    print(" ")

# ---------------------------
# Create new structure

print("Creating Structure...")
new_md_path = os.path.join(contentpath, new_file_path)
new_img_path = os.path.join(assetspath, new_file_path, md_name.split('.')[0])
new_img_path_md = new_file_path_md + '/' + md_name.split('.')[0]
new_img_dir = os.path.join(new_img_path, "img")
new_mat_dir = os.path.join(new_img_path, "mat")
print(">>>>  New markdown path: " + new_md_path)
print(">>>>  New image path: " + new_img_dir)
print(">>>>  New material path: " + new_mat_dir)

print("-----------------------------------------------------------------------")

#..... replace links in md file

# Read the document out
f = open(md_path, 'r')
content = f.read()
content = content.replace("%20", " ")
f.close()
# ---------------------------
# Rewrite Markdown file Links
md_string = content

def FindUrl(string):
    # findall() has been used
    # with valid conditions for urls in string
    regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    url = re.findall(regex, string)
    if url:
        # return [x[0] for x in url]
        return True
    else:
        return False

# print("URL is: " + str(FindUrl("https://mir-s3-cdn-cf.behance.net/project_modules/1400/65cd0f19610111.562dd492dd5ba.jpg")))

# if do_load_images:

## get all positions of images in string
print("Searching all image links in file...")
# find all start index of images_references and store it ('![')
# img_idx_lst = [i for i in range(len(md_string)) if md_string.startswith(img_pre_strt_str, i)]
for i in range(len(md_string)):
    if md_string.startswith(img_pre_strt_str, i):
        img_idx_lst.append(i)
print(">>>>  Found " + str(len(img_idx_lst)) + " images in the document.")
# print("Img start positions:\t" + str(img_idx_lst))

# find all pre ends ('(') of images_references and store it if they appear after the according opening item
indcnt = 0
for i in range(len(md_string)):
    if (md_string.startswith(img_pre_end_str, i) and indcnt < len(img_idx_lst) and i > img_idx_lst[indcnt]):
        img_idx_pre_lst.append(i)
        indcnt += 1
# print("Img link positions:\t" + str(img_idx_pre_lst))

# find all ends of images_references and store it (')')
indcnt = 0
for i in range(len(md_string)):
    if (md_string.startswith(img_end_str, i) and indcnt < len(img_idx_pre_lst) and i > img_idx_pre_lst[indcnt]):
        img_idx_end_lst.append(i)
        indcnt += 1
# print("Img end positions:\t" + str(img_idx_end_lst))

print("-----------------------------------------------------------------------")
print("Replaceing the image strings...")
# iterate through all the image references in the text and replace them
for idx in range(len(img_idx_lst)):
    # print(idx)
    # print("INDEX-------------------------------------------------------")
    for idx2 in range(img_idx_lst[idx] + len(img_pre_strt_str), img_idx_pre_lst[idx]):
        ext_img_str = ext_img_str + md_string[idx2]
        img_pre_str = img_pre_strt_str + ext_img_str + img_pre_end_str
    # print("Pre-String: " + img_pre_str)
    if not (img_pre_str == "![]("): print(str(idx) + " Alt-Text-String: " + img_pre_str)
    old_img = ""
    for idx3 in range(img_idx_lst[idx] + len(img_pre_str), img_idx_end_lst[idx]):
        old_img_path = old_img_path + md_string[idx3]
        old_img = old_img + md_string[idx3]
    print(str(idx) + " Old Image Path: " + old_img_path)
    # print("URL is: " + str(FindUrl(old_img_path)))    
    if not FindUrl(old_img_path): new_img_path_md_img = r'%assets_url%' + new_img_path_md + '/img/' + os.path.basename(old_img)
    else: new_img_path_md_img = old_img_path
    referenced_img_names.append(os.path.basename(new_img_path_md_img))
    old_img_ref = img_pre_str + old_img_path + img_end_str
    new_img_ref = img_pre_str + new_img_path_md_img + img_end_str
    old_img_ref_lst.append(old_img_ref) 
    new_img_ref_lst.append(new_img_ref)
    print(str(idx) + " New Image Path: " + new_img_path_md_img)
    
    # reset vars
    ext_img_str = ''
    img_pre_str = ''
    old_img_path = ''
    new_img_path_md_img = ''
    old_img_ref = ''
    new_img_ref = ''

for i in range(len(old_img_ref_lst)):
    md_string = md_string.replace(old_img_ref_lst[i], new_img_ref_lst[i])

print("-----------------------------------------------------------------------")
# print("new test string: " + md_string)
# print("-----------------------------------------------------------------------")

#..... check if folder exists
#..... not: create folder

# The new MD-Folder
if not os.path.isdir(new_md_path):
    os.makedirs(new_md_path)

## The new img folder in Assets/Post
if not os.path.isdir(new_img_dir):
    os.makedirs(new_img_dir)

## The new mat folder in Assets/Post
if not os.path.isdir(new_mat_dir):
    os.makedirs(new_mat_dir)

#..... check if file exists
#..... not: create file
#..... replace text in md file
print("Creating the markdown file...")

# Create the new markdown file
try:
    new_md_file = open(os.path.join(new_md_path, md_name), 'w+')
    new_md_file.write(md_string)
    new_md_file.close()
except OSError as error:
    print(error)

print("-----------------------------------------------------------------------")
#..... copy images
if do_load_images:
    print("Copying images to new directory...")

    ecnt = 0
    for filename in os.listdir(image_folder_path):
        # FIND WAY TO NOT COPY EVERY IMAGE
        for i in referenced_img_names:
            if(filename == i):
                print(str(ecnt) + " Copy from\t " + os.path.join(image_folder_path, filename))
                print("   Copy to\t " + os.path.join(new_img_dir, filename))
                copied_img_names.append(filename)
                ecnt+=1
                # Try Copy Image
                try:
                    shutil.copyfile(os.path.join(image_folder_path, filename), os.path.join(new_img_dir, filename)) 
                except OSError as error:
                    print(error)
    # print("-----------------------------------------------------------------------")
    # print("All copied image:")   
    # print(copied_img_names)    
    # print("-----------------------------------------------------------------------")

print("-----------------------------------------------------------------------")
print("Look at your folder, files and folders should be created.")
print("-----------------------------------------------------------------------")

os.system("pause")