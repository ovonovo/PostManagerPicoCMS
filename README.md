# PostManagerPicoCMS

Helps to copy markdown notes from your own note structure to pico cms. It creates a similar folder structure in content and asset folder, copies the images and changes the links in your markdown file to make them fit.

Copy the `AddMarkdownPos.py` file to the content folder of picoCMS and run it. Then drag you markdown file and one image of your image folder to the opening terminal window and optional give it a name. It creates folder and files like this:

```markdown
Pico-CMS-Folder/
├─ assets/
│  ├─ myTopic/
│  │  ├─ post1/
│  │  │  ├─ img/
│  │  │  │  ├─ image1.png
│  │  │  │  ├─ image2.jpg
│  │  │  │  ├─ image3.bmp
│  │  │  ├─ mat/
│  │  │  │  ├─ downloadfile.mp4
│  │  │  │  ├─ downloadfile.pdf
│  │  ├─ post2/
│  │  │  ├─ img/
│  │  │  │  ├─ image1.png
│  │  │  │  ├─ image2.jpg
│  │  │  │  ├─ image3.bmp
│  │  │  ├─ mat/
│  │  │  │  ├─ downloadfile.mp4
│  │  │  │  ├─ downloadfile.pdf
├─ config/
├─ content/
│  ├─ AddMarkdownPost.py
│  ├─ index.md
│  ├─ myTopic/
│  │  ├─ index.md
│  │  ├─ post1.md
│  │  ├─ post2.md
├─ plugins/
├─ .gitignore
├─ README.md

```

In the markdown file all local image-links will be replaced with correct references to assets folder.

*Before*
```markdown
# My Headline

Some text and an image.

![alt text](img/image1.png)
```

*After*
```markdown
# My Headline

Some text and an image.

![alt text](%assets%/myTopic/post1/img/image1.png)
```

